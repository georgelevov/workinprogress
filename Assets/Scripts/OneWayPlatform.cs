﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPlatform : MonoBehaviour
{
    public Transform Player;
    public Transform Parent;
    // Start is called before the first frame update
    private bool oneWay = false;
    void Start()
    {

    }
    
    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.name == "Player")
            oneWay = true;
        //Debug.Log("Collided");
        if (gameObject.transform.position.y > Player.gameObject.transform.position.y)
        {

        //   Debug.Log("Player entered the collision below platform");
            Physics.IgnoreCollision(Player.GetComponent<Collider>(), Parent.GetComponent<Collider>());

        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.name == "Player")
            oneWay = false;
        //Debug.Log("Exit Collision");
        if (gameObject.transform.position.y <= Player.gameObject.transform.position.y)
        {
        //    Debug.Log("Player exited the collision above platform");
            Physics.IgnoreCollision(Player.GetComponent<Collider>(), Parent.GetComponent<Collider>(), false);
        }
    }
}

