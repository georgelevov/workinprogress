﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveElevator : MonoBehaviour
{
    Rigidbody rigidBody;
    public float topPosition;
    public float bottomPosition;
    public float speed;
    public bool isMovingBottom;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //Debug.Log(rigidBody.transform.position.y);
        float position = rigidBody.transform.position.y;
        if (isMovingBottom && position > bottomPosition)
        {
            rigidBody.MovePosition(transform.position + Time.deltaTime * speed *
                transform.TransformDirection(0f, -1f, 0f));
        }
        else if (isMovingBottom && position <= bottomPosition) {
            isMovingBottom = false;
            rigidBody.MovePosition(transform.position + Time.deltaTime * speed *
                transform.TransformDirection(0f, 1f, 0f));
            return;
        }
        if (!isMovingBottom && position < topPosition)
        {
            rigidBody.MovePosition(transform.position + Time.deltaTime * speed *
                transform.TransformDirection(0f, 1f, 0f));
        }
        else if (!isMovingBottom && position >= topPosition)
        {
            isMovingBottom = true;
                rigidBody.MovePosition(transform.position + Time.deltaTime * speed *
            transform.TransformDirection(0f, -1f, 0f));
        }


    }
}
