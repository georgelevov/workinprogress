﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public Text scoreText;
    public GameObject breakObjects;
    public float scoreIncrease;
    public static float score;
    private float timePassed;
    private List<GameObject> breakableObjects = new List<GameObject>();
    private List<Object> objectScripts = new List<Object>();

    void Start()
    {
        ObjectBreaker objectBreaker = breakObjects.transform.GetComponent<ObjectBreaker>();
        breakableObjects = objectBreaker.breakableObjects;
        foreach (GameObject breakableObject in breakableObjects)
        {
            objectScripts.Add(breakableObject.transform.GetComponent<Object>());
        }
    }

    void Update()
    {
        timePassed += Time.deltaTime;
        if (timePassed >= scoreIncrease)
        {
            List<Object> objectScriptsNotBroken = objectScripts.FindAll(el => el.isBroken != true);
            score += timePassed * ((float)objectScriptsNotBroken.Count / (float)objectScripts.Count);
            timePassed -= (int) timePassed;
            scoreText.text = "Distance: " + score.ToString("n2");
        }
    }
}
