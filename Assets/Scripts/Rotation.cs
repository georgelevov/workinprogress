﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{

    public float angle;
    public float timeInterval;
        // Use this for initialization
        void Start()
        {
            StartCoroutine(RotateObject(angle, Vector3.forward, timeInterval));
        }

        IEnumerator RotateObject(float angle, Vector3 axis, float inTime)
        {
            // calculate rotation speed
            float rotationSpeed = angle / inTime;

            while (true)
            {
                // save starting rotation position
                Quaternion startRotation = transform.rotation;

                float deltaAngle = 0;

                // rotate until reaching angle
                while (deltaAngle < angle)
                {
                    deltaAngle += rotationSpeed * Time.deltaTime;
                    deltaAngle = Mathf.Min(deltaAngle, angle);

                    transform.rotation = startRotation * Quaternion.AngleAxis(deltaAngle, axis);

                    yield return null;
                }

                // delay here
                //yield return new WaitForSeconds(1);
            }
        }
    }