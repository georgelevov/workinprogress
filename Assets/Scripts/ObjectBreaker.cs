﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectBreaker : MonoBehaviour
{
    public float breakTime;
    public float breakDecreaseTime;
    public float breakDecrease;
    public float breakBottom;
    private float timePassedBreakDecrease;
    private float timePassedBreak;
    public List<GameObject> breakableObjects = new List<GameObject>();
    private List<Object> objectScripts = new List<Object>();

    private AudioSource audioSource;

    public AudioClip[] breakSources;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        foreach (GameObject breakableObject in breakableObjects) {
            objectScripts.Add(breakableObject.transform.GetComponent<Object>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        timePassedBreakDecrease += Time.deltaTime;
        timePassedBreak += Time.deltaTime;

        if (timePassedBreakDecrease >= breakDecreaseTime)
        {
            
            timePassedBreakDecrease -= (int)timePassedBreakDecrease;
            if (breakTime > breakBottom) {
                breakTime -= breakDecrease;
            }
            Debug.Log("Break time is " + breakTime);
        }

        if (timePassedBreak >= breakTime)
        {
            List<Object> objectScriptsNotBroken = objectScripts.FindAll(el => el.isBroken != true);
            if (objectScriptsNotBroken.Count > 1)
            {
                System.Random rnd = new System.Random();
                int breakIndex = rnd.Next(0, objectScriptsNotBroken.Count);
                objectScriptsNotBroken[breakIndex].isBroken = true;
                objectScriptsNotBroken[breakIndex].Smoke.SetActive(true);
                Debug.Log(objectScriptsNotBroken[breakIndex].name + "is broken");

                audioSource.clip = breakSources[Random.Range(0, breakSources.Length)];
                audioSource.Play();

            }
            else SceneManager.LoadScene(2);
            timePassedBreak -= (int)timePassedBreak;
        }
    }
}
