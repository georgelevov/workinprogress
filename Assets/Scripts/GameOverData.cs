﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverData : MonoBehaviour
{
    // Start is called before the first frame update

    public float finalScore;
    private Text scoreText;

    Scene scene;
    private void OnLevelWasLoaded(int level)
    {
        scene = SceneManager.GetActiveScene();
    }


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        finalScore = ScoreScript.score;

        if (scene.name == "Game Over")
        {
            scoreText = GameObject.Find("ScoreText").GetComponent<Text>();

            //Debug.Log(finalScore);

            scoreText.text = finalScore.ToString();
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

}
