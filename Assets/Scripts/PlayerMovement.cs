﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float runSpeed;
    public float jumpForce;
    Rigidbody rigidBody;
    private float horizontalAxis;
    private float verticalAxis;
    private bool playerIsJumping;
    public Animator playerAnimator;
    public GameObject llamaModel;

    private AudioSource audioSource;

    public AudioClip[] jumpSources;


    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        horizontalAxis = Input.GetAxis("Horizontal");
        verticalAxis = Input.GetAxis("Vertical");
        if (!playerIsJumping && Input.GetKeyDown(KeyCode.W))
        {
            playerIsJumping = true;
            playerAnimator.SetTrigger("isJumpingAnim");

            audioSource.clip = jumpSources[Random.Range(0, jumpSources.Length)];
            audioSource.Play();

        }

        if (rigidBody.velocity.y < -0.1f)
        {
            playerAnimator.SetBool("isFallingAnim", true);
        }
        else if (rigidBody.velocity.y > -0.1f) playerAnimator.SetBool("isFallingAnim", false);

        if (Input.GetKey("a") || Input.GetKey("d"))
        {
            playerAnimator.SetBool("isWalkingAnim", true);
        }
        else playerAnimator.SetBool("isWalkingAnim", false);

        if (Input.GetKey("a"))
        {
            llamaModel.transform.eulerAngles = new Vector3(0, -270, 0);
        }
        if (Input.GetKey("d"))
        {
            llamaModel.transform.eulerAngles = new Vector3(0, 270, 0);
        }



    }

    void FixedUpdate()
    {
        rigidBody.MovePosition(transform.position + Time.deltaTime * runSpeed *
            transform.TransformDirection(horizontalAxis, 0f, 0f));
        if (playerIsJumping)
        {
            rigidBody.MovePosition(transform.position + Time.deltaTime * jumpForce *
            transform.TransformDirection(horizontalAxis, 1f, 0f));
        }


    }

    void OnCollisionEnter(Collision theCollision)
    {
        if (theCollision.gameObject.tag == "Platform")
        {
            playerIsJumping = false;
        }
    }
}
