﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomOutCamera : MonoBehaviour
{
    public CameraFollow cameraFollow;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == "Player")
        {
            cameraFollow.isFollow = false;
            cameraFollow.transform.position = cameraFollow.transform.position + new Vector3(0f, -35f, -85f);
            cameraFollow.transform.eulerAngles = new Vector3(0f, 0f, 0f);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name == "Player")
        {
            cameraFollow.isFollow = true;
            cameraFollow.transform.eulerAngles = new Vector3(0f, 0f, 0f);
        }
    }
}
