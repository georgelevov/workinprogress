﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoaderScript : MonoBehaviour
{
    Object parentScript;

    // Start is called before the first frame update
    void Start()
    {
        parentScript = gameObject.transform.GetComponentInParent<Object>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void repairObject()
    {
        parentScript.isBroken = false;
        parentScript.Smoke.SetActive(false);
        parentScript.LoadBar.SetActive(false);

    }
}
