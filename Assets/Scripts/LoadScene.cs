﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public string sceneToLoad;
    public float countDownTimer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(countDownTimer);
        countDownTimer = countDownTimer - Time.deltaTime;
        if (countDownTimer <= 0)
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}
