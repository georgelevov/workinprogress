﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour
{
    public GameObject LoadBar;
    public GameObject Loader;
    public GameObject Smoke;
    public GameObject Player;

    public bool isBroken = false;
    private Animation loadAnimation;

    private AudioSource audioSource;

    public AudioClip[] repairSources;


    // Start is called before the first frame update
    void Start()
    {
        Loader.SetActive(false);
        LoadBar.SetActive(false);
        loadAnimation = LoadBar.GetComponent<Animation>();
        audioSource = GetComponent<AudioSource>();
    }

        // Update is called once per frame
        void Update()
    {

    }


    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player" && isBroken)
        {

            Loader.SetActive(true);
            LoadBar.SetActive(true);

            audioSource.clip = repairSources[Random.Range(0, repairSources.Length)];
            audioSource.Play(0);

            StartLoaderCount();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.name == "Player")
        {
            loadAnimation.Stop("Loading");

        }
    }

    private void StartLoaderCount()
    {
        loadAnimation.Play("Loading");
    }
}
