// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Amplify_PL/Water_1"
{
	Properties
	{
		_DeepColor("DeepColor", Color) = (0,0.2631485,0.5660378,0)
		_ShallowColor("ShallowColor", Color) = (0,0.6109539,0.8396226,0)
		_WaterNormal("Water Normal", 2D) = "bump" {}
		_WaterFallOff("WaterFallOff", Float) = 0
		_WaterDepth("WaterDepth", Float) = 0.9
		_Distortion("Distortion", Float) = 0.5
		_Ambient("Ambient", Float) = 0.6
		_NormalScale("NormalScale", Float) = 0.3
		_FoamDepth("FoamDepth", Float) = 0
		_FoamFallOff("FoamFallOff", Float) = 0
		_FoamSmoothnes("FoamSmoothnes", Float) = 0
		_FoamSpecular("FoamSpecular", Float) = 0
		_Foam("Foam", 2D) = "white" {}
		_WaterSpecular("WaterSpecular", Float) = 0
		_WaterSmoothness("WaterSmoothness", Float) = 0
		_Tiling("Tiling", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Background"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		GrabPass{ }
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf StandardSpecular keepalpha 
		struct Input
		{
			half2 uv_texcoord;
			float4 screenPos;
		};

		uniform sampler2D _WaterNormal;
		uniform half _NormalScale;
		uniform half2 _Tiling;
		uniform half4 _DeepColor;
		uniform half4 _ShallowColor;
		UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
		uniform float4 _CameraDepthTexture_TexelSize;
		uniform half _WaterDepth;
		uniform half _WaterFallOff;
		uniform half _FoamDepth;
		uniform half _FoamFallOff;
		uniform sampler2D _Foam;
		uniform float4 _Foam_ST;
		uniform sampler2D _GrabTexture;
		uniform half _Distortion;
		uniform half _WaterSpecular;
		uniform half _FoamSpecular;
		uniform half _WaterSmoothness;
		uniform half _FoamSmoothnes;
		uniform half _Ambient;


		inline float4 ASE_ComputeGrabScreenPos( float4 pos )
		{
			#if UNITY_UV_STARTS_AT_TOP
			float scale = -1.0;
			#else
			float scale = 1.0;
			#endif
			float4 o = pos;
			o.y = pos.w * 0.5f;
			o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
			return o;
		}


		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float2 uv_TexCoord9 = i.uv_texcoord * _Tiling;
			float2 panner10 = ( 1.0 * _Time.y * float2( 0.04,0 ) + uv_TexCoord9);
			float2 panner11 = ( 1.0 * _Time.y * float2( 0.02,0.04 ) + uv_TexCoord9);
			float3 temp_output_15_0 = BlendNormals( UnpackScaleNormal( tex2D( _WaterNormal, panner10 ), _NormalScale ) , UnpackScaleNormal( tex2D( _WaterNormal, panner11 ), _NormalScale ) );
			o.Normal = temp_output_15_0;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float eyeDepth2 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD( ase_screenPos ))));
			float temp_output_4_0 = abs( ( eyeDepth2 - ase_screenPos.w ) );
			float temp_output_28_0 = saturate( pow( ( temp_output_4_0 + _WaterDepth ) , _WaterFallOff ) );
			float4 lerpResult7 = lerp( _DeepColor , _ShallowColor , temp_output_28_0);
			float4 color41 = IsGammaSpace() ? half4(1,1,1,0) : half4(1,1,1,0);
			float2 uv0_Foam = i.uv_texcoord * _Foam_ST.xy + _Foam_ST.zw;
			float2 panner36 = ( 1.0 * _Time.y * float2( -0.01,0.01 ) + uv0_Foam);
			float temp_output_38_0 = ( saturate( pow( ( temp_output_4_0 + _FoamDepth ) , _FoamFallOff ) ) * tex2D( _Foam, panner36 ).r );
			float4 lerpResult39 = lerp( lerpResult7 , color41 , temp_output_38_0);
			float4 ase_grabScreenPos = ASE_ComputeGrabScreenPos( ase_screenPos );
			float4 ase_grabScreenPosNorm = ase_grabScreenPos / ase_grabScreenPos.w;
			float4 screenColor17 = tex2D( _GrabTexture, ( half3( (ase_grabScreenPosNorm).xy ,  0.0 ) + ( temp_output_15_0 * _Distortion ) ).xy );
			float4 lerpResult23 = lerp( lerpResult39 , screenColor17 , temp_output_28_0);
			o.Albedo = lerpResult23.rgb;
			float lerpResult46 = lerp( _WaterSpecular , _FoamSpecular , temp_output_38_0);
			half3 temp_cast_3 = (lerpResult46).xxx;
			o.Specular = temp_cast_3;
			float lerpResult47 = lerp( _WaterSmoothness , _FoamSmoothnes , temp_output_38_0);
			o.Smoothness = lerpResult47;
			o.Occlusion = _Ambient;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16700
217;73;1076;583;857.9689;96.04691;1;True;False
Node;AmplifyShaderEditor.ScreenPosInputsNode;1;-4025.961,290.432;Float;False;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenDepthNode;2;-3689.522,252.1488;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;50;-3224.052,-573.781;Float;False;Property;_Tiling;Tiling;16;0;Create;True;0;0;False;0;0,0;5,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleSubtractOpNode;3;-3410.605,480.0245;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-2915.419,-590.19;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.AbsOpNode;4;-3157.207,474.5555;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;10;-2530.832,-609.5071;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.04,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-1889.085,631.7888;Float;False;Property;_WaterDepth;WaterDepth;5;0;Create;True;0;0;False;0;0.9;1.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-2458.204,-308.2235;Float;False;Property;_NormalScale;NormalScale;8;0;Create;True;0;0;False;0;0.3;1.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;11;-2476.394,-463.7504;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.02,0.04;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-2194.626,1099.733;Float;False;Property;_FoamDepth;FoamDepth;9;0;Create;True;0;0;False;0;0;0.95;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;12;-2201.504,-687.4684;Float;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Instance;13;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;13;-2192.79,-464.1602;Float;True;Property;_WaterNormal;Water Normal;3;0;Create;True;0;0;False;0;dd2fd2df93418444c8e280f1d34deeb5;dd2fd2df93418444c8e280f1d34deeb5;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;31;-1972.626,1083.733;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;35;-2243.626,1351.733;Float;False;0;37;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;25;-1826.085,420.7888;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;27;-1680.085,563.7888;Float;False;Property;_WaterFallOff;WaterFallOff;4;0;Create;True;0;0;False;0;0;-3.59;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;33;-2036.626,1214.733;Float;False;Property;_FoamFallOff;FoamFallOff;10;0;Create;True;0;0;False;0;0;4.52;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;15;-1873.089,-543.1201;Float;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GrabScreenPosition;42;-1786.322,-962.5679;Float;False;0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;32;-1776.626,1083.733;Float;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;26;-1636.085,461.7888;Float;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;36;-1971.626,1359.733;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.01,0.01;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;22;-1570.35,-576.2879;Float;False;Property;_Distortion;Distortion;6;0;Create;True;0;0;False;0;0.5;0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-1510.772,-705.6669;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;34;-1566.626,1095.733;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;5;-1831.812,58.2191;Float;False;Property;_DeepColor;DeepColor;0;0;Create;True;0;0;False;0;0,0.2631485,0.5660378,0;0.08370417,0.408316,0.5377358,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;37;-1748.943,1324.72;Float;True;Property;_Foam;Foam;13;0;Create;True;0;0;False;0;None;5228a04ef529d2641937cab585cc1a02;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;28;-1473.08,322.0816;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;6;-1827.234,225.5532;Float;False;Property;_ShallowColor;ShallowColor;2;0;Create;True;0;0;False;0;0,0.6109539,0.8396226,0;0,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;43;-1501.794,-912.8072;Float;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-1200.928,1323.591;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;7;-1107.803,171.6851;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;41;-1205.958,663.2985;Float;False;Constant;_Color0;Color 0;7;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;20;-1179.772,-797.6672;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;49;-200.3416,836.9551;Float;False;Property;_FoamSmoothnes;FoamSmoothnes;11;0;Create;True;0;0;False;0;0;8.08;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-356.4893,472.7768;Float;False;Property;_WaterSpecular;WaterSpecular;14;0;Create;True;0;0;False;0;0;-0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-213.6816,743.5749;Float;False;Property;_WaterSmoothness;WaterSmoothness;15;0;Create;True;0;0;False;0;0;-2.38;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;17;-895.5651,-658.9797;Float;False;Global;_GrabScreen0;Grab Screen 0;3;0;Create;True;0;0;False;0;Object;-1;False;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;39;-833.8853,668.8358;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;45;-343.1493,566.157;Float;False;Property;_FoamSpecular;FoamSpecular;12;0;Create;True;0;0;False;0;0;-0.04;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;23;-166.4707,-487.8065;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;46;-100.0644,441.6501;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-49.29721,190.1422;Float;False;Property;_Ambient;Ambient;7;0;Create;True;0;0;False;0;0.6;0.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;47;42.74328,712.4482;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;458.3184,-1.443264;Half;False;True;2;Half;ASEMaterialInspector;0;0;StandardSpecular;Amplify_PL/Water_1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;False;Background;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;1;0
WireConnection;3;0;2;0
WireConnection;3;1;1;4
WireConnection;9;0;50;0
WireConnection;4;0;3;0
WireConnection;10;0;9;0
WireConnection;11;0;9;0
WireConnection;12;1;10;0
WireConnection;12;5;14;0
WireConnection;13;1;11;0
WireConnection;13;5;14;0
WireConnection;31;0;4;0
WireConnection;31;1;30;0
WireConnection;25;0;4;0
WireConnection;25;1;24;0
WireConnection;15;0;12;0
WireConnection;15;1;13;0
WireConnection;32;0;31;0
WireConnection;32;1;33;0
WireConnection;26;0;25;0
WireConnection;26;1;27;0
WireConnection;36;0;35;0
WireConnection;21;0;15;0
WireConnection;21;1;22;0
WireConnection;34;0;32;0
WireConnection;37;1;36;0
WireConnection;28;0;26;0
WireConnection;43;0;42;0
WireConnection;38;0;34;0
WireConnection;38;1;37;1
WireConnection;7;0;5;0
WireConnection;7;1;6;0
WireConnection;7;2;28;0
WireConnection;20;0;43;0
WireConnection;20;1;21;0
WireConnection;17;0;20;0
WireConnection;39;0;7;0
WireConnection;39;1;41;0
WireConnection;39;2;38;0
WireConnection;23;0;39;0
WireConnection;23;1;17;0
WireConnection;23;2;28;0
WireConnection;46;0;44;0
WireConnection;46;1;45;0
WireConnection;46;2;38;0
WireConnection;47;0;48;0
WireConnection;47;1;49;0
WireConnection;47;2;38;0
WireConnection;0;0;23;0
WireConnection;0;1;15;0
WireConnection;0;3;46;0
WireConnection;0;4;47;0
WireConnection;0;5;29;0
ASEEND*/
//CHKSM=6161A4AE35BEF65F5463FCCA0766E17FD0821685