// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Ground"
{
	Properties
	{
		_SpecColor("Specular Color",Color)=(1,1,1,1)
		_Ground("Ground", 2D) = "white" {}
		_Normalstrangth("Normal strangth", Float) = 0
		_Tileing("Tileing", Float) = 3.32
		_Gloss("Gloss", Float) = 0
		_Color0("Color 0", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		ZTest LEqual
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf BlinnPhong keepalpha addshadow fullforwardshadows 
		struct Input
		{
			half2 uv_texcoord;
		};

		uniform sampler2D _Ground;
		uniform half _Tileing;
		uniform half _Normalstrangth;
		uniform half4 _Color0;
		uniform half _Gloss;

		void surf( Input i , inout SurfaceOutput o )
		{
			half2 temp_cast_0 = (_Tileing).xx;
			float2 uv_TexCoord23 = i.uv_texcoord * temp_cast_0;
			float2 temp_output_2_0_g1 = uv_TexCoord23;
			float2 break6_g1 = temp_output_2_0_g1;
			float temp_output_25_0_g1 = ( pow( 0.5 , 3.0 ) * 0.1 );
			float2 appendResult8_g1 = (half2(( break6_g1.x + temp_output_25_0_g1 ) , break6_g1.y));
			half4 tex2DNode14_g1 = tex2D( _Ground, temp_output_2_0_g1 );
			float temp_output_4_0_g1 = _Normalstrangth;
			float3 appendResult13_g1 = (half3(1.0 , 0.0 , ( ( tex2D( _Ground, appendResult8_g1 ).g - tex2DNode14_g1.g ) * temp_output_4_0_g1 )));
			float2 appendResult9_g1 = (half2(break6_g1.x , ( break6_g1.y + temp_output_25_0_g1 )));
			float3 appendResult16_g1 = (half3(0.0 , 1.0 , ( ( tex2D( _Ground, appendResult9_g1 ).g - tex2DNode14_g1.g ) * temp_output_4_0_g1 )));
			float3 normalizeResult22_g1 = normalize( cross( appendResult13_g1 , appendResult16_g1 ) );
			o.Normal = normalizeResult22_g1;
			o.Albedo = _Color0.rgb;
			o.Specular = tex2D( _Ground, uv_TexCoord23 ).r;
			o.Gloss = _Gloss;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16700
-1920;0;1920;1019;1404.37;819.1487;1.34788;True;False
Node;AmplifyShaderEditor.RangedFloatNode;24;-336.4795,296.8279;Float;False;Property;_Tileing;Tileing;5;0;Create;True;0;0;False;0;3.32;30.17;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;13;-738.9728,-441.778;Float;True;Property;_Ground;Ground;3;0;Create;True;0;0;False;0;e2b5c1a96cac1fa449c63f4f03da178c;1adac7733c236a34981e96982d7b1e27;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;23;-109.6995,268.9429;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;10,10;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;23.88179,100.7705;Float;False;Property;_Normalstrangth;Normal strangth;4;0;Create;True;0;0;False;0;0;2.7;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;28;118.7348,-466.0041;Float;False;Property;_Color0;Color 0;7;0;Create;True;0;0;False;0;0,0,0,0;1,0.9970345,0.783,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;25;-426.5818,-184.4883;Float;True;Property;_TextureSample3;Texture Sample 3;4;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;21;312.5428,63.71488;Float;True;NormalCreate;0;;1;e12f7ae19d416b942820e3932b56220f;0;4;1;SAMPLER2D;;False;2;FLOAT2;0,0;False;3;FLOAT;0.5;False;4;FLOAT;10.57;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;26;478.6189,-100.7286;Float;False;Property;_Gloss;Gloss;6;0;Create;True;0;0;False;0;0;3.8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;906.1837,-265.8405;Half;False;True;2;Half;ASEMaterialInspector;0;0;BlinnPhong;Ground;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;3;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;4;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;1;False;-1;1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;2;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;23;0;24;0
WireConnection;25;0;13;0
WireConnection;25;1;23;0
WireConnection;21;1;13;0
WireConnection;21;2;23;0
WireConnection;21;4;19;0
WireConnection;0;0;28;0
WireConnection;0;1;21;0
WireConnection;0;3;25;0
WireConnection;0;4;26;0
ASEEND*/
//CHKSM=97A94844FEA9F7665564012A2A350354A286097A