// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SoftBodNormal"
{
	Properties
	{
		_SpecColor("Specular Color",Color)=(1,1,1,1)
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 13
		_TessMin( "Tess Min Distance", Float ) = 10
		_TessMax( "Tess Max Distance", Float ) = 8.52
		_AnimTime("AnimTime", Float) = 0.2
		_Health("Health", Float) = 0.27
		_AntiRotateTime("AntiRotateTime", Float) = -0.1
		_Remapper("Remapper", Float) = -0.21
		_Albedo("Albedo", 2D) = "white" {}
		_MainColor("MainColor", Color) = (1,0.0990566,0.0990566,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		ZTest LEqual
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Tessellation.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		struct Input
		{
			float3 worldNormal;
			float3 worldPos;
			half2 uv_texcoord;
		};

		uniform half _AntiRotateTime;
		uniform half _AnimTime;
		uniform half _Remapper;
		uniform half4 _MainColor;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform half _Health;
		uniform half _TessValue;
		uniform half _TessMin;
		uniform half _TessMax;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }

		float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }

		float snoise( float3 v )
		{
			const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
			float3 i = floor( v + dot( v, C.yyy ) );
			float3 x0 = v - i + dot( i, C.xxx );
			float3 g = step( x0.yzx, x0.xyz );
			float3 l = 1.0 - g;
			float3 i1 = min( g.xyz, l.zxy );
			float3 i2 = max( g.xyz, l.zxy );
			float3 x1 = x0 - i1 + C.xxx;
			float3 x2 = x0 - i2 + C.yyy;
			float3 x3 = x0 - 0.5;
			i = mod3D289( i);
			float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
			float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
			float4 x_ = floor( j / 7.0 );
			float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
			float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 h = 1.0 - abs( x ) - abs( y );
			float4 b0 = float4( x.xy, y.xy );
			float4 b1 = float4( x.zw, y.zw );
			float4 s0 = floor( b0 ) * 2.0 + 1.0;
			float4 s1 = floor( b1 ) * 2.0 + 1.0;
			float4 sh = -step( h, 0.0 );
			float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
			float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
			float3 g0 = float3( a0.xy, h.x );
			float3 g1 = float3( a0.zw, h.y );
			float3 g2 = float3( a1.xy, h.z );
			float3 g3 = float3( a1.zw, h.w );
			float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
			g0 *= norm.x;
			g1 *= norm.y;
			g2 *= norm.z;
			g3 *= norm.w;
			float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
			m = m* m;
			m = m* m;
			float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
			return 42.0 * dot( m, px);
		}


		float4 tessFunction( appdata_full v0, appdata_full v1, appdata_full v2 )
		{
			return UnityDistanceBasedTess( v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue );
		}

		void vertexDataFunc( inout appdata_full v )
		{
			float3 ase_vertex3Pos = v.vertex.xyz;
			float2 NoiseSize71 = half2( 20,20 );
			float2 uv_TexCoord58 = v.texcoord.xy * NoiseSize71;
			float mulTime57 = _Time.y * _AntiRotateTime;
			float cos60 = cos( mulTime57 );
			float sin60 = sin( mulTime57 );
			float2 rotator60 = mul( uv_TexCoord58 - half2( 0.5,0.5 ) , float2x2( cos60 , -sin60 , sin60 , cos60 )) + half2( 0.5,0.5 );
			float simplePerlin2D63 = snoise( ( half3( rotator60 ,  0.0 ) + ase_vertex3Pos ).xy );
			float antiRotTime62 = simplePerlin2D63;
			float2 uv_TexCoord18 = v.texcoord.xy * NoiseSize71;
			float mulTime54 = _Time.y * _AnimTime;
			float cos50 = cos( mulTime54 );
			float sin50 = sin( mulTime54 );
			float2 rotator50 = mul( uv_TexCoord18 - half2( 0.5,0.5 ) , float2x2( cos50 , -sin50 , sin50 , cos50 )) + half2( 0.5,0.5 );
			float simplePerlin3D20 = snoise( ( half3( rotator50 ,  0.0 ) + ase_vertex3Pos ) );
			float myNoise38 = simplePerlin3D20;
			float MixedNoise68 = ( antiRotTime62 * myNoise38 );
			v.vertex.xyz += ( ase_vertex3Pos * MixedNoise68 * _Remapper );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			half3 ase_worldNormal = i.worldNormal;
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			half3 temp_cast_0 = (ase_worldlightDir.x).xxx;
			float dotResult104 = dot( ase_worldNormal , temp_cast_0 );
			float2 NoiseSize71 = half2( 20,20 );
			float2 uv_TexCoord58 = i.uv_texcoord * NoiseSize71;
			float mulTime57 = _Time.y * _AntiRotateTime;
			float cos60 = cos( mulTime57 );
			float sin60 = sin( mulTime57 );
			float2 rotator60 = mul( uv_TexCoord58 - half2( 0.5,0.5 ) , float2x2( cos60 , -sin60 , sin60 , cos60 )) + half2( 0.5,0.5 );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float simplePerlin2D63 = snoise( ( half3( rotator60 ,  0.0 ) + ase_vertex3Pos ).xy );
			float antiRotTime62 = simplePerlin2D63;
			float2 uv_TexCoord18 = i.uv_texcoord * NoiseSize71;
			float mulTime54 = _Time.y * _AnimTime;
			float cos50 = cos( mulTime54 );
			float sin50 = sin( mulTime54 );
			float2 rotator50 = mul( uv_TexCoord18 - half2( 0.5,0.5 ) , float2x2( cos50 , -sin50 , sin50 , cos50 )) + half2( 0.5,0.5 );
			float simplePerlin3D20 = snoise( ( half3( rotator50 ,  0.0 ) + ase_vertex3Pos ) );
			float myNoise38 = simplePerlin3D20;
			float MixedNoise68 = ( antiRotTime62 * myNoise38 );
			float temp_output_84_0 = (-0.49 + (MixedNoise68 - 0.0) * (1.74 - -0.49) / (1.0 - 0.0));
			float highlights85 = temp_output_84_0;
			half3 temp_cast_4 = (highlights85).xxx;
			float temp_output_93_0 = saturate( ( 1.0 - ( ( distance( temp_cast_4 , float3( 0,0,0 ) ) - 0.27 ) / max( 0.0 , 1E-05 ) ) ) );
			float Highlights105 = temp_output_93_0;
			half3 temp_cast_5 = (( dotResult104 * Highlights105 )).xxx;
			half3 temp_cast_6 = (( dotResult104 * Highlights105 )).xxx;
			float3 linearToGamma135 = LinearToGammaSpace( temp_cast_6 );
			float3 litHighlights110 = linearToGamma135;
			float lightNoise44 = (0.31 + (MixedNoise68 - 0.0) * (1.32 - 0.31) / (1.0 - 0.0));
			float2 uv0_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float temp_output_120_0 = saturate( ( 1.0 - ( ( distance( float3( 0,0,0 ) , float3( 0,0,0 ) ) - (0.98 + (MixedNoise68 - 0.0) * (2.54 - 0.98) / (1.0 - 0.0)) ) / max( 0.0 , 1E-05 ) ) ) );
			float HardShadows118 = temp_output_120_0;
			float4 temp_output_87_0 = ( half4( litHighlights110 , 0.0 ) + ( ( lightNoise44 * ( _MainColor * tex2D( _Albedo, uv0_Albedo ) ) ) * HardShadows118 ) );
			o.Albedo = temp_output_87_0.rgb;
			float4 color138 = IsGammaSpace() ? half4(0.04739182,0.1981132,0,0) : half4(0.003705362,0.03251993,0,0);
			float4 color146 = IsGammaSpace() ? half4(1,0.244495,0,0) : half4(1,0.04870002,0,0);
			float4 lerpResult145 = lerp( color138 , color146 , ( _Health - 0.3 ));
			o.Emission = ( lerpResult145 * temp_output_87_0 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf BlinnPhong keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc tessellate:tessFunction 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.6
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16700
-1920;0;1920;1019;1451.365;660.8993;1.689026;True;False
Node;AmplifyShaderEditor.Vector2Node;55;-2175.046,1168.39;Float;False;Constant;_NoiseSize;NoiseSize;7;0;Create;True;0;0;False;0;20,20;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RegisterLocalVarNode;71;-1957.61,1171.219;Float;False;NoiseSize;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;53;-1225.558,804.3519;Float;False;Property;_AnimTime;AnimTime;8;0;Create;True;0;0;False;0;0.2;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;72;-1444.57,1391.901;Float;False;71;NoiseSize;1;0;OBJECT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;56;-1419.192,1663.253;Float;False;Property;_AntiRotateTime;AntiRotateTime;10;0;Create;True;0;0;False;0;-0.1;0.001;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;73;-1347.041,553.8118;Float;False;71;NoiseSize;1;0;OBJECT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;54;-1056.202,798.7064;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;58;-1165.601,1391.605;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;18;-1093.121,527.31;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;51;-1129.589,663.2214;Float;False;Constant;_Vector1;Vector 1;9;0;Create;True;0;0;False;0;0.5,0.5;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleTimeNode;57;-1128.682,1663.002;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;59;-1202.069,1527.517;Float;False;Constant;_Vector3;Vector 3;9;0;Create;True;0;0;False;0;0.5,0.5;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.PosVertexDataNode;65;-868.5377,1672.429;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;49;-1214.039,1061.64;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RotatorNode;50;-824.7473,544.6717;Float;True;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RotatorNode;60;-897.2275,1408.967;Float;True;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;64;-570.5709,1473.709;Float;True;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;48;-581.1508,894.1207;Float;True;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;20;-339.4118,662.4495;Float;True;Simplex3D;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;63;-299.6561,1439.916;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;38;-17.43657,592.7694;Float;True;myNoise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;62;-43.19424,1441.166;Float;False;antiRotTime;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;67;518.2798,1231.092;Float;False;38;myNoise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;66;522.6614,1145.605;Float;False;62;antiRotTime;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;795.0248,1140.707;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;68;1040.587,1136.719;Float;True;MixedNoise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;81;338.1442,1924.52;Float;False;Constant;_Float0;Float 0;10;0;Create;True;0;0;False;0;1.74;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;82;355.0968,1723.802;Float;False;68;MixedNoise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;83;320.0076,1815.699;Float;False;Constant;_Float1;Float 1;10;0;Create;True;0;0;False;0;-0.49;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;84;577.2351,1790.723;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;85;864.7965,1800.65;Float;False;highlights;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;86;-359.7342,-720.5292;Float;False;85;highlights;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;94;-340.752,-624.8194;Float;False;Constant;_Float4;Float 4;11;0;Create;True;0;0;False;0;0.27;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;93;-122.5856,-694.2523;Float;True;Color Mask;-1;;1;eec747d987850564c95bde0e5a6d1867;0;4;1;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;114;-260.6958,2379.844;Float;False;Constant;_Float7;Float 7;10;0;Create;True;0;0;False;0;0.98;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;103;581.1765,-1021.692;Float;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;70;-1146.884,1977.403;Float;False;68;MixedNoise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;115;-225.6066,2287.947;Float;False;68;MixedNoise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-1163.837,2178.122;Float;False;Constant;_max;max;10;0;Create;True;0;0;False;0;1.32;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;102;607.6843,-1198.411;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;116;-263.3793,2488.665;Float;False;Constant;_Float8;Float 8;10;0;Create;True;0;0;False;0;2.54;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;105;181.1159,-567.7054;Float;False;Highlights;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;42;-1181.974,2069.301;Float;False;Constant;_min;min;10;0;Create;True;0;0;False;0;0.31;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;104;869.3074,-1193.543;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;41;-928.0606,2045.982;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;106;773.7071,-831.2751;Float;True;105;Highlights;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;17;-1016.736,-72.31985;Float;False;0;9;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;117;-6.782703,2356.525;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;107;1170.167,-848.3091;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;10;-505.058,-441.5712;Float;False;Property;_MainColor;MainColor;15;0;Create;True;0;0;False;0;1,0.0990566,0.0990566,0;0.5188676,0.09055676,0.09055676,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;120;273.9321,2163.657;Float;True;Color Mask;-1;;4;eec747d987850564c95bde0e5a6d1867;0;4;1;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;44;-573.8091,2078.888;Float;False;lightNoise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;9;-560.7905,-261.6702;Float;True;Property;_Albedo;Albedo;12;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LinearToGammaNode;135;1413.514,-1022.978;Float;True;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;39;-15.45129,-424.2612;Float;False;44;lightNoise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-119.7905,-259.6702;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;118;1065.411,2594.75;Float;True;HardShadows;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;110;1679.009,-869.6099;Float;True;litHighlights;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;142;-76.91742,339.8652;Float;False;Property;_Health;Health;9;0;Create;True;0;0;False;0;0.27;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;125;244.3242,-22.25975;Float;False;118;HardShadows;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;230.7987,-300.1255;Float;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;138;338.5507,204.0051;Float;False;Constant;_Color1;Color 1;16;0;Create;True;0;0;False;0;0.04739182,0.1981132,0,0;1,0.04705883,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;151;170.6229,286.2305;Float;False;2;0;FLOAT;0;False;1;FLOAT;0.3;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;146;342.8209,395.5225;Float;False;Constant;_Color2;Color 2;12;0;Create;True;0;0;False;0;1,0.244495,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;111;526.0221,-320.5982;Float;False;110;litHighlights;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;127;496.0791,-52.3256;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.PosVertexDataNode;152;690.1693,560.9321;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;69;419.0447,627.569;Float;True;68;MixedNoise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;87;788.4506,-313.0071;Float;True;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;145;688.0496,196.82;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;25;377.9695,849.6809;Float;False;Property;_Remapper;Remapper;11;0;Create;True;0;0;False;0;-0.21;-0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;100;-795.741,-1436.84;Float;False;Constant;_Float5;Float 5;11;0;Create;True;0;0;False;0;0.57;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;90;668.2827,2007.757;Float;False;Constant;_Float2;Float 2;11;0;Create;True;0;0;False;0;0.08;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;97;-473.1927,-1467.278;Float;True;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;76;-25.08421,-347.371;Float;False;68;MixedNoise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;12;-566.9902,-73.27022;Float;True;Property;_Normal;Normal;13;0;Create;True;0;0;False;0;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;123;348.6736,2567.226;Float;False;Constant;_Color0;Color 0;11;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;147;1030.019,-77.37726;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;852.3931,2730.907;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalVertexDataNode;46;-1214.696,907.0587;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;89;902.2175,1956.258;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;99;226.5464,-1189.863;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;91;667.8678,2088.965;Float;False;Constant;_Float3;Float 3;11;0;Create;True;0;0;False;0;0.49;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-207.6883,94.73589;Float;False;Property;_Specular;Specular;7;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;121;601.0882,2399.3;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;101;-139.5076,-1470.322;Float;True;Color Mask;-1;;5;eec747d987850564c95bde0e5a6d1867;0;4;1;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;6;-528.7906,325.7297;Float;False;Property;_LightThrough;LightThrough;6;0;Create;True;0;0;False;0;0,0,0,0;0.5188676,0.09055691,0.09055691,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;136;838.4284,423.4006;Float;False;110;litHighlights;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;153;1080.615,492.3597;Float;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;7;-567.9905,129.1298;Float;True;Property;_Depth;Depth;14;0;Create;True;0;0;False;0;None;31cafcc7840963f43ab15238af6cc7eb;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-93.79053,185.3298;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1406.868,-91.36943;Half;False;True;6;Half;ASEMaterialInspector;0;0;BlinnPhong;SoftBodNormal;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;3;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;True;0;13;10;8.52;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;1;False;-1;1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;1;0;False;0;0;False;-1;0;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;71;0;55;0
WireConnection;54;0;53;0
WireConnection;58;0;72;0
WireConnection;18;0;73;0
WireConnection;57;0;56;0
WireConnection;50;0;18;0
WireConnection;50;1;51;0
WireConnection;50;2;54;0
WireConnection;60;0;58;0
WireConnection;60;1;59;0
WireConnection;60;2;57;0
WireConnection;64;0;60;0
WireConnection;64;1;65;0
WireConnection;48;0;50;0
WireConnection;48;1;49;0
WireConnection;20;0;48;0
WireConnection;63;0;64;0
WireConnection;38;0;20;0
WireConnection;62;0;63;0
WireConnection;61;0;66;0
WireConnection;61;1;67;0
WireConnection;68;0;61;0
WireConnection;84;0;82;0
WireConnection;84;3;83;0
WireConnection;84;4;81;0
WireConnection;85;0;84;0
WireConnection;93;3;86;0
WireConnection;93;4;94;0
WireConnection;105;0;93;0
WireConnection;104;0;102;0
WireConnection;104;1;103;1
WireConnection;41;0;70;0
WireConnection;41;3;42;0
WireConnection;41;4;43;0
WireConnection;117;0;115;0
WireConnection;117;3;114;0
WireConnection;117;4;116;0
WireConnection;107;0;104;0
WireConnection;107;1;106;0
WireConnection;120;4;117;0
WireConnection;44;0;41;0
WireConnection;9;1;17;0
WireConnection;135;0;107;0
WireConnection;11;0;10;0
WireConnection;11;1;9;0
WireConnection;118;0;120;0
WireConnection;110;0;135;0
WireConnection;37;0;39;0
WireConnection;37;1;11;0
WireConnection;151;0;142;0
WireConnection;127;0;37;0
WireConnection;127;1;125;0
WireConnection;87;0;111;0
WireConnection;87;1;127;0
WireConnection;145;0;138;0
WireConnection;145;1;146;0
WireConnection;145;2;151;0
WireConnection;97;2;100;0
WireConnection;12;1;17;0
WireConnection;147;0;145;0
WireConnection;147;1;87;0
WireConnection;124;1;123;0
WireConnection;89;0;84;0
WireConnection;89;1;90;0
WireConnection;89;2;91;0
WireConnection;99;0;101;0
WireConnection;99;1;93;0
WireConnection;121;0;120;0
WireConnection;101;1;97;0
WireConnection;153;0;152;0
WireConnection;153;1;69;0
WireConnection;153;2;25;0
WireConnection;8;0;7;0
WireConnection;8;1;6;0
WireConnection;0;0;87;0
WireConnection;0;2;147;0
WireConnection;0;11;153;0
ASEEND*/
//CHKSM=DE117D900EC8489825F7F52E6C0A8780E6D6EFF2